import { createStore } from "vuex";

export const key= Symbol()

export const store =  createStore({
    state: {
        count: 0,
        sidebar: false
    },
    getters: {
        getSidebar:state => {
            return state.sidebar;
        },
    },
    mutations: {
        TOGGLE_SIDEBAR(state){
            state.sidebar = !state.sidebar;
        },
        CLOSE_SIDEBAR(state) {
            state.sidebar = false;
        }
    },
    actions: {
        toggleSideBar(context){
            context.commit("TOGGLE_SIDEBAR")
        },
        closeSideBar(){
            context.commit("CLOSE_SIDEBAR")
        }
    }
})