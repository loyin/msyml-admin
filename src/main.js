import { createApp } from 'vue'
import App from './App.vue'
// 路由
import router from './routers'
// 状态管理
import { store, key } from './store'
// 全局样式
import "styles/index.scss"
import "./static/iconfont.css"
// v-md-editor
import vmdeditor from 'plugins/mdeditor.js'
// naive-ui
import naive from 'plugins/naive.js'

const app =  createApp(App)
app.use(router)
app.use(store, key)
app.use(vmdeditor)
app.use(naive)
app.mount('#app')
