import {
  // create naive ui
  create,
  // component
  NButton,
  NDropdown,
  NMenu,
  NIcon,
  NBreadcrumb
} from 'naive-ui'

const naive = create({
  components: [NButton, NDropdown, NMenu, NIcon, NBreadcrumb]
})

export default function (app) {
  app.use(naive)
}
